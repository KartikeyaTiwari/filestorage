package com.example.admin.file;
// /A
import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class View_PDF_Files extends AppCompatActivity {

    ListView myPDFListView;
    EditText searh;
    private String Tag="Null";


    private ArrayAdapter<String> adapter;

    DatabaseReference databaseReference;
    List<uploadPdf> uploadPdfs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view__pdf__files);

        myPDFListView=(ListView)findViewById(R.id.myListView);
        searh=findViewById(R.id.inputSearch);

        if (searh == null) {
            Log.e(Tag, "null");
        }
        uploadPdfs=new ArrayList<>();

       // viewAllFiles();

        searh.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                (View_PDF_Files.this).adapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });



        myPDFListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @SuppressLint("IntentReset")
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                com.example.admin.file.uploadPdf uploadPdf = uploadPdfs.get(i);


                Intent intent =new Intent();
                intent.setType(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(uploadPdf.getUrl()));
                startActivity(intent);

            }
        });

        databaseReference =FirebaseDatabase.getInstance().getReference(Constants.DATABASE_PATH_UPLOADS);
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for(DataSnapshot postSnapshot :dataSnapshot.getChildren())
                {
                    uploadPdf uploadPDF =postSnapshot.getValue(uploadPdf.class);


                    uploadPdfs.add(uploadPDF);

                }

                String[] uploads =new String[uploadPdfs.size()];

                for(int i=0;i<uploads.length;i++)
                {
                    uploads[i]= uploadPdfs.get(i).getName();

                }
                adapter =new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_list_item_1,uploads);
                myPDFListView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    }

